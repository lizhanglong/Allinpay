﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Allinpay;
using Allinpay.Model;
using IDotNet;
using log4net;
using Xunit;

namespace UnitTest
{
    public class TestAllipay
    {
        protected string Card_Id = "8668083660000002247";
        private const string Password = "111111";
        private const string ProductId = "0001";
        protected string Opr_Id;
        protected static string PayResultOrderId;
        protected static string PayResultOrderTime;
        protected AllinpayHelper allinpayHelper;
        protected ILog log;

        public TestAllipay()
        {
            Opr_Id = "UnitTest";
            allinpayHelper = new AllinpayHelper();
            log = LogManager.GetLogger("");
            AllinpayLib.LogEventHandler = (tag, content) => log.DebugFormat("tag:{0}\ncontent:{1}", tag.ToString(), content);
            log.Debug("begin--------------------------");
        }

        private string GetShortGuidString(Guid uid, bool useBase32 = false)
        {
            if (useBase32)
                return Base32Encoding.ToBase32String(uid.ToByteArray());
            else
                return Convert.ToBase64String(uid.ToByteArray());
        }

        [Fact]
        public void PayWithoutPassword()
        {
            var orderId = GetShortGuidString(Guid.NewGuid(), true);
            var mer_order_id = GetShortGuidString(Guid.NewGuid(), true);
            var info = new PayWithoutPasswordParamInfo(orderId, mer_order_id, 1, Card_Id)
            {
            };
            var d = allinpayHelper.PayWithoutPassword(info);
            Assert.True(d.IsSuccess, d.LogMsg);
            PayResultOrderId = mer_order_id;
            PayResultOrderTime = info.mer_tm;
        }

        [Fact]
        public void PayWithPassword()
        {
            var orderId = GetShortGuidString(Guid.NewGuid(), true);
            var mer_order_id = GetShortGuidString(Guid.NewGuid(), true);
            var info = new PayWithPasswordParamInfo(orderId, mer_order_id, 1, Card_Id, Password)
            {
                //  payment_id = "0000000002"
                // chan_no = "4000000002"
            };
            var d = allinpayHelper.PayWithPassword(info);
            Assert.True(d.IsSuccess, d.LogMsg);
            PayResultOrderId = mer_order_id;
            PayResultOrderTime = info.mer_tm;
        }

        [Fact]
        public void GetCardInfo()
        {
            var orderId = GetShortGuidString(Guid.NewGuid(), true);
            var d = allinpayHelper.GetCardInfo(new GetCardInfoParamInfo()
            {
                card_id = Card_Id,
                password = Password
            });
            Assert.True(d.IsSuccess, d.LogMsg);
        }

        [Fact]
        public void GetCardInfoNoPassword()
        {
            var orderId = GetShortGuidString(Guid.NewGuid(), true);
            var d = allinpayHelper.GetCardInfoNoPassword(new GetCardInfoNoPasswordParamInfo()
            {
                card_id = Card_Id,
                order_id = orderId,
            });
            Assert.True(d.IsSuccess, d.LogMsg);
        }

        [Fact]
        public void GetPayResult()
        {
            PayWithPassword();
            Assert.True(PayResultOrderId != null, "PayResultOrderId is null");
            Assert.True(PayResultOrderTime != null, "PayResultOrderTime is null");
            var d = allinpayHelper.GetPayResult(new GetPayResultParamInfo()
            {
                mer_order_id = PayResultOrderId,
                mer_tm = PayResultOrderTime,
            });
            Assert.True(d.IsSuccess, d.LogMsg);
        }

        [Fact]
        public void SingleTopUp()
        {
            var orderId = GetShortGuidString(Guid.NewGuid(), true);
            var d = allinpayHelper.SingleTopUp(new CardSingleTopUpParamInfo()
            {
                card_id = Card_Id,
                amount = "1",
                order_id = orderId,
                opr_id = Opr_Id,
                prdt_no = ProductId,
                top_up_way = TopUpWayType.其它.ToString("d"),
            });
            Assert.True(d.IsSuccess, d.LogMsg);
        }

        [Fact]
        public void ChangeCardPassword()
        {
            var orderId = GetShortGuidString(Guid.NewGuid(), true);
            var newPwd = "222222";
            var d = allinpayHelper.ChangeCardPassword(new CardChangePasswordParamInfo()
            {
                order_id = orderId,
                card_id = Card_Id,
                password = Password,
                new_password = newPwd
            });
            Assert.True(d.IsSuccess, d.LogMsg);
            orderId = GetShortGuidString(Guid.NewGuid(), true);
            var d2 = allinpayHelper.ChangeCardPassword(new CardChangePasswordParamInfo()
            {
                order_id = orderId,
                card_id = Card_Id,
                password = newPwd,
                new_password = Password
            });
            Assert.True(d2.IsSuccess, d2.LogMsg);
        }

        [Fact]
        public void SearchCardRecordLogWithPassword()
        {
            var orderId = GetShortGuidString(Guid.NewGuid(), true);
            var d = allinpayHelper.SearchCardRecordLogWithPassword(new CardSearchRecordWithPasswordParamInfo()
            {
                card_id = Card_Id,
                password = Password,
                begin_date = DateTime.Now.AddMonths(-2).ToString("yyyyMMdd"),
                end_date = DateTime.Now.ToString("yyyyMMdd"),
                page_no = 1,
                page_size = 30
            });
            Assert.True(d.IsSuccess, d.LogMsg);
        }


    }
}
