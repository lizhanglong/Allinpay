﻿using System;
using System.Collections.Generic;
using System.Text;
using Allinpay.Model;
namespace Allinpay
{
    /// <summary>
    /// 通联接口
    /// </summary>
    public class AllinpayHelper
    {
        #region Core
        /// <summary>
        /// des加密
        /// </summary>
        /// <param name="info"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected virtual string CrptoDESEncrypt(BaseParamInfo info, string value)
        {
            return AllinpayLib.CrptoDESEncrypt("{0}aop{1}".FormatWith(info.timestamp, value));
        }
        #endregion

        #region 根据卡号密码查询卡片信息
        /// <summary>
        /// 根据卡号密码查询卡片信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public CheckResultResponseInfo<CardCardinfoResponseInfo> GetCardInfo(GetCardInfoParamInfo info)
        {
            info.password = CrptoDESEncrypt(info, info.password);
            var data = AllinpayLib.Request<GetCardInfoParamInfo, CardCardinfoResponse>(info);
            AllinpayLib.Log(data);
            return data.CheckResultResponse(info);
        }

        /// <summary>
        /// 根据卡号密码查询卡片信息
        /// </summary>
        /// <param name="cardId">19位卡号</param>
        /// <param name="password">6位数字密码</param>
        /// <returns></returns>
        public CheckResultResponseInfo<CardCardinfoResponseInfo> GetCardInfo(string cardId, string password)
        {
            return GetCardInfo(new GetCardInfoParamInfo()
            {
                card_id = cardId,
                password = password
            });
        }
        #endregion

        #region 无密码查询卡片信息

        /// <summary>
        /// 根据卡号无密码查询卡片信息
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public CheckResultResponseInfo<CardCardinfoResponseInfo> GetCardInfoNoPassword(GetCardInfoNoPasswordParamInfo info)
        {
            var data = AllinpayLib.Request<GetCardInfoNoPasswordParamInfo, GetCardInfoNoPasswordResponse>(info);
            AllinpayLib.Log(data);
            return data.CheckResultResponse(info);
        }

        /// <summary>
        /// 根据卡号无密码查询卡片信息
        /// </summary>
        /// <param name="cardId">19位卡号</param>
        /// <returns></returns>
        public CheckResultResponseInfo<CardCardinfoResponseInfo> GetCardInfoNoPassword(string cardId)
        {
            //5+19+6=30
            var orderId = "NoPwd{0}{1}".FormatWith(DateTime.Now.ToString("yyyyMMddHHmmssfffff"), AllinpayLib.Random.Next(100000, 1000000).ToString());
            return GetCardInfoNoPassword(orderId, cardId);
        }

        /// <summary>
        /// 根据卡号无密码查询卡片信息
        /// </summary>
        /// <param name="orderId">订单号(仅仅是接口请求的标识，防止重复提交用的，没有业务含义)
        ///  交易唯一流水号，用于控制交易的唯一性（只能是数字或英文） 最长30位</param>
        /// <param name="cardId">19位卡号</param>
        /// <returns></returns>
        public CheckResultResponseInfo<CardCardinfoResponseInfo> GetCardInfoNoPassword(string orderId, string cardId)
        {
            return GetCardInfoNoPassword(new GetCardInfoNoPasswordParamInfo()
            {
                order_id = orderId,
                card_id = cardId
            });
        }

        #endregion

        #region 单卡充值

        /// <summary>
        /// 单卡充值（根据充值卡号、产品、充值金额、充值途径、备注等信息进行卡充值）
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public CheckResultResponseInfo<CardSingleTopUpAddResponseInfo> SingleTopUp(CardSingleTopUpParamInfo info)
        {
            var data = AllinpayLib.Request<CardSingleTopUpParamInfo, CardSingleTopUpAddResponse>(info);
            AllinpayLib.Log(data);
            return data.CheckResultResponse(info);
        }

        /// <summary>
        /// 单卡充值（根据充值卡号、产品、充值金额、充值途径、备注等信息进行卡充值）
        /// </summary>
        /// <param name="order_id">交易唯一流水号，用于控制交易的唯一性（只能是数字或英文）</param>
        /// <param name="card_id">充值卡号  19位卡号 </param>
        /// <param name="prdt_no">产品号</param>
        /// <param name="amount">充值金额  10000 以分为单位</param>
        /// <param name="opr_id">充值机构或充值人员</param>
        /// <param name="desn">备注</param>
        /// <returns></returns>
        public CheckResultResponseInfo<CardSingleTopUpAddResponseInfo> SingleTopUp(string order_id, string card_id, string prdt_no, string amount, string opr_id, string desn)
        {
            return SingleTopUp(new CardSingleTopUpParamInfo()
            {
                order_id = order_id,
                card_id = card_id,
                prdt_no = prdt_no,
                amount = amount,
                opr_id = opr_id,
                desn = desn
            });
        }
        #endregion

        #region 用于网上支付，需卡密验证
        /// <summary>
        /// 用于网上支付，需卡密验证
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public CheckResultResponseInfo<CardPayWithPasswordResponseInfo> PayWithPassword(PayWithPasswordParamInfo info)
        {
            info.card_id = CrptoDESEncrypt(info, info.card_id);
            info.password = CrptoDESEncrypt(info, info.password);
            var data = AllinpayLib.Request<PayWithPasswordParamInfo, CardPayWithPasswordResponse>(info);
            AllinpayLib.Log(data);
            return data.CheckResultResponse(info);
        }
        #endregion

        #region 卡无磁无密支付接口(签约代扣)
        /// <summary>
        /// 卡无磁无密支付接口(签约代扣)
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public CheckResultResponseInfo<PayWithoutPasswordResponseInfo> PayWithoutPassword(PayWithoutPasswordParamInfo info)
        {
            info.card_id = CrptoDESEncrypt(info, info.card_id);
            var data = AllinpayLib.Request<PayWithoutPasswordParamInfo, PayWithoutPasswordResponse>(info);
            AllinpayLib.Log(data);
            return data.CheckResultResponse(info);
        }
        #endregion

        #region 单笔支付查询
        /// <summary>
        /// 单笔支付查询
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public CheckResultResponseInfo<GetPayResultResponseInfo> GetPayResult(GetPayResultParamInfo info)
        {
            var data = AllinpayLib.Request<GetPayResultParamInfo, GetPayQueryResponse>(info);
            AllinpayLib.Log(data);
            return data.CheckResultResponse(info);
        }

        /// <summary>
        /// 单笔支付交易查询(查询w开头的表) 查询的是allinpay.card. paywithpassword.add  卡密支付接口结果记录
        /// </summary>
        /// <param name="mer_order_id">商户订单号</param>
        /// <param name="mer_tm">订单交易时间 yyyyMMddHHmmss</param>
        /// <returns></returns>
        public CheckResultResponseInfo<GetPayResultResponseInfo> GetPayResult(string mer_order_id, string mer_tm)
        {
            var info = new GetPayResultParamInfo
            {
                mer_order_id = mer_order_id,
                mer_tm = mer_tm,
            };

            return GetPayResult(info);
        }

        #endregion

        #region 修改卡密码
        /// <summary>
        /// 修改卡密码
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public CheckResultResponseInfo<CardChangePasswordResponseInfo> ChangeCardPassword(CardChangePasswordParamInfo info)
        {
            info.password = CrptoDESEncrypt(info, info.password);
            info.new_password = CrptoDESEncrypt(info, info.new_password);
            var data = AllinpayLib.Request<CardChangePasswordParamInfo, CardChangePasswordResponse>(info);
            AllinpayLib.Log(data);
            return data.CheckResultResponse(info);
        }
        #endregion

        #region 持卡人交易查询(需卡密码)
        /// <summary>
        /// 持卡人交易查询(需卡密码)
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public CheckResultResponseInfo<CardSearchRecordWithPasswordResponseInfo> SearchCardRecordLogWithPassword(CardSearchRecordWithPasswordParamInfo info)
        {
            info.password = CrptoDESEncrypt(info, info.password);
            var data = AllinpayLib.Request<CardSearchRecordWithPasswordParamInfo, CardSearchRecordWithPasswordResponse>(info);
            AllinpayLib.Log(data);
            return data.CheckResultResponse(info);
        }
        #endregion


    }
}
