﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    /// <summary>
    /// 平台级错误参数，主要是验证参数的是否为空，合法性，以及接入安全的校验等；
    /// 50以内的为接入平台的错误，50以上的为业务平台的父级错误。
    /// 接入平台错误只返回code 和msg,不返回 sub_code 和sub_msg
    /// </summary>
    public class ErrorCodeInfo : ErrorSubCodeInfo
    {
        /// <summary>
        /// 错误代号
        /// </summary>
        public string code { get; set; }

        /// <summary>
        /// 错误英文
        /// </summary>
        public string msg_en { get; set; }

        /// <summary>
        /// 错误中文
        /// </summary>
        public string msg_cn { get; set; }

        /// <summary>
        /// 解决方案
        /// </summary>
        public string remark { get; set; }


    }
}
