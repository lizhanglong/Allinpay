﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Allinpay.Model
{
    /// <summary>
    /// 交易流水
    /// </summary>
    public class CardRecordLogInfo
    {
        /// <summary>
        /// 发卡机构号
        /// </summary>
        public string open_brh_id { get; set; }

        /// <summary>
        /// 卡号
        /// </summary>
        public string card_id { get; set; }

        /// <summary>
        /// 产品号
        /// </summary>
        public string prdt_no { get; set; }

        /// <summary>
        /// 卡品牌
        /// </summary>
        public string brand_id { get; set; }

        /// <summary>
        /// 交易日期
        /// </summary>
        public string int_txn_dt { get; set; }

        /// <summary>
        /// 交易时间
        /// </summary>
        public string int_txn_tm { get; set; }

        /// <summary>
        /// 商户号
        /// </summary>
        public string accept_brh_id { get; set; }

        /// <summary>
        /// 交易流水
        /// </summary>
        public string int_txn_seq_id { get; set; }

        /// <summary>
        /// 交易参考号
        /// </summary>
        public string access_ref_seq_id { get; set; }

        /// <summary>
        /// 交易金额
        /// </summary>
        public string txn_at { get; set; }

        /// <summary>
        /// 手续费
        /// </summary>
        public string txn_fee_at { get; set; }

        /// <summary>
        /// 账户余额
        /// </summary>
        public string acct_bal_at { get; set; }

        /// <summary>
        /// 账户可用余额
        /// </summary>
        public string avail_bal_at { get; set; }

        /// <summary>
        /// 交易状态：
        /// 0-挂起
        ///1-失败
        ///2-成功
        ///3-已冲正
        ///4-撤销
        /// </summary>
        public string txn_sta_cd { get; set; }

        /// <summary>
        /// 终端号
        /// </summary>
        public string term_id { get; set; }

        /// <summary>
        /// 交易码:
        /// B0010-消费
        ///B0011-消费撤销
        ///B0071-退货确认
        ///B0012-消费冲正
        ///B0013-消费撤销冲正
        ///B0020-网上支付
        /// </summary>
        public string txn_cd { get; set; }
    }
}
