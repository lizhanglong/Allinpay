﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    public class CardSingleTopUpAddResponse : BaseResponse<CardSingleTopUpAddResponseInfo>
    {
        public CardSingleTopUpAddResponseInfo ppcs_cardsingletopup_add_response { get; set; }

        protected override CardSingleTopUpAddResponseInfo GetResultInfo()
        {
            return ppcs_cardsingletopup_add_response;
        }
    }

    public class CardSingleTopUpAddResponseInfo:BaseResultInfo
    {
        /// <summary>
        /// 单卡充值结果信息
        /// </summary>
        public SingleTopUpResultInfo result_info { get; set; }

        /// <summary>
        /// 合作机构上送订单号
        /// </summary>
        public string order_id { get; set; }


        /// <summary>
        /// 流水号
        /// </summary>
        public string trans_no { get; set; }
        
    }

}
