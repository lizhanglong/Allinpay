﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    public class GetCardInfoNoPasswordResponse : BaseResponse<CardCardinfoResponseInfo>
    {
        public CardCardinfoResponseInfo ppcs_cardinfo_get_response { get; set; }

        protected override CardCardinfoResponseInfo GetResultInfo()
        {
            return ppcs_cardinfo_get_response;
        }
    }
}
