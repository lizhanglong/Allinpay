﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    /// <summary>
    ///  单卡充值
    /// </summary>
    public class CardSingleTopUpParamInfo : BaseHandleOrderParamInfo
    {

        public CardSingleTopUpParamInfo()
        {
            top_up_way = TopUpWayType.其它.ToString("d");
        }

        public override string method
        {
            get { return "allinpay.ppcs.cardsingletopup.add"; }
        }

        ///<summary>
        ///必须 充值卡号  19位卡号 
        ///</summary>
        public String card_id { get; set; }

        ///<summary>
        ///必须 产品号   
        ///</summary>
        public String prdt_no { get; set; }

        ///<summary>
        ///必须 充值金额  10000 以分为单位
        ///</summary>
        public String amount { get; set; }

        ///<summary>
        ///必须 充值途径
        ///</summary>
        public string top_up_way { get; set; }

        ///<summary>
        ///必须 充值机构或充值人员   
        ///</summary>
        public String opr_id { get; set; }

        ///<summary>
        ///备注   
        ///</summary>
        public String desn { get; set; }
    }
}
