﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    /// <summary>
    /// 业务平台错误
    /// </summary>
    public class ErrorSubCodeInfo
    {
        /// <summary>
        ///业务平台错误码
        /// </summary>
        public string sub_code { get; set; }

        /// <summary>
        /// 业务平台错误码信息
        /// </summary>
        public string sub_msg { get; set; }
    }
}
