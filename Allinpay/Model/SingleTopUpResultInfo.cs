﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    public class SingleTopUpResultInfo
    {
        ///<summary>
        ///卡号  
        ///</summary>
        public String card_id { get; set; }

        ///<summary>
        ///产品号  
        ///</summary>
        public String prdt_no { get; set; }

        ///<summary>
        ///充值金额 100.00
        ///</summary>
        public String amount { get; set; }

        ///<summary>
        ///充值途径 1
        ///</summary>
        public TopUpWayType top_up_way { get; set; }

        ///<summary>
        ///备注  
        ///</summary>
        public String desn { get; set; }

        ///<summary>
        ///账户余额 100.10
        ///</summary>
        public String account_balance { get; set; }

        ///<summary>
        ///可用余额 100.00
        ///</summary>
        public String valid_balance { get; set; }

        ///<summary>
        ///有效期 20121231
        ///</summary>
        public String validity_date { get; set; }
    }
}
