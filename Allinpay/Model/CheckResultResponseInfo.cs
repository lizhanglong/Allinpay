﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    public class CheckResultResponseInfo<T>
    {
        /// <summary>
        /// 判断请求是否成功
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// 显示给用户的消息
        /// </summary>
        public string ShowUserMsg { get; set; }

        /// <summary>
        /// 用于记录日志的消息
        /// </summary>
        public string LogMsg { get; set; }

        public T ResultInfo { get; set; }


    }
}
