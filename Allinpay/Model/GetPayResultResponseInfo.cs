﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{

    public class GetPayQueryResponse : BaseResponse<GetPayResultResponseInfo>
    {

        public GetPayResultResponseInfo card_payresult_get_response { get; set; }

        protected override GetPayResultResponseInfo GetResultInfo()
        {
            return card_payresult_get_response;
        }
    }
    public class GetPayResultResponseInfo : BaseResultInfo
    {

        public PayOrderInfo pay_order_info { get; set; }
    }

    public class PayOrderInfo 
    {
        /// <summary>
        /// 商户号
        /// </summary>
        public string mer_id { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public int stat { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string reason { get; set; }

        /// <summary>
        /// 货币单位 rmb
        /// </summary>
        public string pay_cur { get; set; }

        /// <summary>
        /// 商户订单号
        /// </summary>
        public string mer_order_id { get; set; }


        /// <summary>
        /// 交易金额
        /// </summary>
        public string amount { get; set; }

        /// <summary>
        ///  00-网上支付查询交易
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// 交易日期 yyyyMMddHHmmss
        /// </summary>
        public string mer_tm { get; set; }

        /// <summary>
        /// 卡号
        /// </summary>
        public string pay_txn_id { get; set; }

        /// <summary>
        /// 卡号
        /// </summary>
        public string pay_txn_tm { get; set; }
    }
}
