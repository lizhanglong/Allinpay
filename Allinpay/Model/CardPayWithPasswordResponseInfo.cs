﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    public class CardPayWithPasswordResponse : BaseResponse<CardPayWithPasswordResponseInfo>
    {
        public CardPayWithPasswordResponseInfo card_paywithpassword_add_response { get; set; }

        protected override CardPayWithPasswordResponseInfo GetResultInfo()
        {
            return card_paywithpassword_add_response;
        }
    }

    public class CardPayWithPasswordResponseInfo : BaseResultInfo
    {
        public string order_id { get; set; }

        public PayResultInfo pay_result_info { get; set; }

    }
}
