﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    public class BaseResultInfo
    {
        /// <summary>
        /// AOP平台时间戳，格式为yyyyMMddHHmmss，例如：20110125010101。
        /// </summary>
        public string res_timestamp { get; set; }

        /// <summary>
        /// API输入参数签名结果，原字符串拼装方式：密钥+ “app_key”+value+”method”+value+” order_id”+value+” res_timestamp”+value+”sign_v”+value+” timestamp”+value+”v”+value+密钥  ,其中 order_id 如查询类交易没有，或为空则不参与原字符串的拼装
        /// </summary>
        public string res_sign { get; set; }

        
    }
}
