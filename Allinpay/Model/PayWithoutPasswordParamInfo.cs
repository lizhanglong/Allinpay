﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    /// <summary>
    /// 卡无磁无密支付接口(签约代扣)
    /// </summary>
    public class PayWithoutPasswordParamInfo : BaseHandleOrderParamInfo
    {
        public PayWithoutPasswordParamInfo()
        {
            type = "01";
            mer_id = AllinpayLib.CompanyNumber;
            mer_tm = DateTime.Now.ToString("yyyyMMddHHmmss");
            pay_cur = "CNY";
            payment_id =AllinpayLib.PaymentId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="order_id">交易唯一流水号，用于控制交易的唯一性（只能是数字或英文）</param>
        /// <param name="mer_order_id">商户订单号 最长30位</param>
        /// <param name="amount">交易金额 100代表1元，以分为单位</param>
        /// <param name="card_id">卡号 19位数字卡号</param>
        public PayWithoutPasswordParamInfo(string order_id, string mer_order_id, ulong amount, string card_id)
            : this()
        {
            this.order_id = order_id;
            this.mer_order_id = mer_order_id;
            this.amount = amount.ToString();
            this.card_id = card_id;
        }

        ///<summary>
        ///交易类型 01-网上支付 01
        ///</summary>
        public String type { get; set; }

        ///<summary>
        ///商户号 通联分配的交易商户  
        ///</summary>
        public String mer_id { get; set; }

        ///<summary>
        ///商户上送交易时间 20110201010101  
        ///</summary>
        public String mer_tm { get; set; }

        ///<summary>
        ///商户订单号 最长30位  
        ///</summary>
        public String mer_order_id { get; set; }

        ///<summary>
        ///支付币种 默认CNY代表人民币  
        ///</summary>
        public String pay_cur { get; set; }

        ///<summary>
        ///支付活动 10位数字 0000000001 (测试环境0000000001 ,生产环境，找通联业务人员分配)
        ///</summary>
        public String payment_id { get; set; }

        ///<summary>
        ///交易金额 100代表1元，以分为单位   
        ///</summary>
        public String amount { get; set; }

        ///<summary>
        ///卡号(DES加密) 19位数字卡号，加密 (发起请求时会自动加密)
        ///</summary>
        public String card_id { get; set; }

        ///<summary>
        ///渠道号
        ///云POS为1000000000
        ///云phone为4000000000，
        ///云网为4000000001，
        ///商家服务台为4000000002
        ///</summary>
        public String chan_no { get; set; }

        /// <summary>
        /// 签约协议号(DES加密) 可选
        /// </summary>
        public string contract_no { get; set; }

        /// <summary>
        /// 合作伙伴签约账号(DES加密)
        /// </summary>
        public string  partner_user{ get; set; }

        ///<summary>
        ///商户上送的自定义信息   
        ///</summary>
        public String misc { get; set; }

        ///<summary>
        ///用户手续费 100代表一元，以分为单位  
        ///</summary>
        public String user_fee { get; set; }

        ///<summary>
        ///商户上送的用户名称  
        ///</summary>
        public String user_name { get; set; }

        ///<summary>
        ///商品名称   
        ///</summary>
        public String goods_name { get; set; }

        ///<summary>
        ///商品种类   
        ///</summary>
        public String goods_type { get; set; }

        ///<summary>
        ///商品号  
        ///</summary>
        public String goods_id { get; set; }

        ///<summary>
        ///商品备注   
        ///</summary>
        public String goods_misc { get; set; }

        public override string method
        {
            get { return "allinpay.card.paywithcontract.add"; }
        }
    }
}
