﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    public class PayWithoutPasswordResponseInfo : BaseResultInfo
    {
        /// <summary>
        /// 交易唯一流水号，用于控制交易的唯一性
        /// </summary>
        public string order_id { get; set; }

        public PayResultInfo pay_result_info { get; set; }
    }
}
