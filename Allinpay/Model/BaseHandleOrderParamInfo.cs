﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    /// <summary>
    /// 所有请求参数中包含order_id的请求
    /// </summary>
    public abstract class BaseHandleOrderParamInfo : BaseParamInfo
    {
        /// <summary>
        /// 交易唯一流水号，用于控制交易的唯一性（只能是数字或英文）
        /// </summary>
        public string order_id { get; set; }
    }
}
