﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    /// <summary>
    ///  单卡充值
    /// </summary>
    public class CardChangePasswordResponse : BaseResponse<CardChangePasswordResponseInfo>
    {
        public CardChangePasswordResponseInfo card_password_change_response { get; set; }

        protected override CardChangePasswordResponseInfo GetResultInfo()
        {
            return card_password_change_response;
        }
    }

    public class CardChangePasswordResponseInfo: BaseResultInfo
    {

        public String card_id { get; set; }

        public string order_id { get; set; }

        public string transNo { get; set; }
    }
}
