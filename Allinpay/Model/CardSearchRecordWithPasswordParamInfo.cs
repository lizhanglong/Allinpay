﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    /// <summary>
    ///   持卡人交易查询
    /// </summary>
    public class CardSearchRecordWithPasswordParamInfo : BaseParamInfo
    {

        public CardSearchRecordWithPasswordParamInfo()
        {
            HttpMethod = "post";
        }

        public override string method
        {
            get { return "allinpay.card.txnlogByCardId.search"; }
        }

        ///<summary>
        ///必须 充值卡号  19位卡号 
        ///</summary>
        public String card_id { get; set; }

        ///<summary>
        ///必须 密码,DES加密   
        ///</summary>
        public String password { get; set; }

        ///<summary>
        ///必须 起始日期
        ///</summary>
        public String begin_date { get; set; }

        ///<summary>
        ///必须 截止日期
        ///</summary>
        public String end_date { get; set; }

        ///<summary>
        ///必须 分页页号
        ///</summary>
        public long page_no { get; set; }

        ///<summary>
        ///必须 单页记录条数
        ///</summary>
        public long page_size { get; set; }

    }
}
