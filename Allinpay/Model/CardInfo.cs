﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
   public class CardInfo
    {
        ///<summary>
        ///卡号 8888888888888888888
        ///</summary>
        public String card_id { get; set; }

        ///<summary>
        ///卡当前所属机构号 0222900003
        ///</summary>
        public String brh_id { get; set; }

        ///<summary>
        ///卡状态 0-正常 1-挂失 2-冻结 3-作废
        ///</summary>
        public String card_stat { get; set; }

        ///<summary>
        ///卡所属品牌 0001-白金卡
        ///</summary>
        public String brand_id { get; set; }

        ///<summary>
        ///有效期 20111230
        ///</summary>
        public String validity_date { get; set; }

       public CardProductInfoArrays card_product_info_arrays { get; set; }
    }

    public class CardProductInfoArrays
    {
        ///<summary>
        ///卡产品列表 可能包含多产品 
        ///</summary>
        public CardProductInfo[] card_product_info { get; set; }
    }
}
