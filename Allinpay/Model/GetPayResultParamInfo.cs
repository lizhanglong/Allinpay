﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    /// <summary>
    /// 单笔支付查询
    /// </summary>
    public class GetPayResultParamInfo : BaseParamInfo
    {

        public GetPayResultParamInfo()
        {
            mer_id = AllinpayLib.CompanyNumber;
            type = "00";
          //  HttpMethod = "post";
        }

        public override string method
        {
            get { return "allinpay.card.payresult.get"; }
        }

        /// <summary>
        /// 商户订单号 最长30位
        /// </summary>
        public string mer_order_id { get; set; }

        /// <summary>
        ///  商户号
        /// </summary>
        public string mer_id { get; set; }

        /// <summary>
        /// 00-网上支付查询交易
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// 交易日期  yyyyMMddHHmmss
        /// </summary>
        public string mer_tm { get; set; }
        //public string trans_date { get; set; }

    }
}
