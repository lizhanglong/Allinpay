﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{

    public class CardSearchRecordWithPasswordResponse : BaseResponse<CardSearchRecordWithPasswordResponseInfo>
    {
        public CardSearchRecordWithPasswordResponseInfo ppcs_txnlog_search_response { get; set; }
        protected override CardSearchRecordWithPasswordResponseInfo GetResultInfo()
        {
            return ppcs_txnlog_search_response;
        }
    }

    /// <summary>
    ///   持卡人交易查询
    /// </summary>
    public class CardSearchRecordWithPasswordResponseInfo : BaseResultInfo
    {
        ///<summary>
        ///  
        ///</summary>
        public long total { get; set; }

        public CardRecordLogArray txn_log_arrays { get; set; }
        
    }

    public class CardRecordLogArray
    {
        ///<summary>
        ///  
        ///</summary>
        public CardRecordLogInfo[] txn_log{ get; set; }
    }
}
