﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
  public  class CardProductInfo
    {
        ///<summary>
        ///卡号 8888888888888888888
        ///</summary>
        public String card_id { get; set; }

        ///<summary>
        ///产品号 0001 (测试固定为0001，生产时分配)
        ///</summary>
        public String product_id { get; set; }

        ///<summary>
        ///产品状态 0
        ///</summary>
        public String product_stat { get; set; }

        ///<summary>
        ///账户余额,两位小数 100
        ///</summary>
        public String account_balance { get; set; }

        ///<summary>
        ///可用余额，两位小数 100
        ///</summary>
        public String valid_balance { get; set; }

        ///<summary>
        ///产品有效期 20111230
        ///</summary>
        public String product_date { get; set; }
    }
}
