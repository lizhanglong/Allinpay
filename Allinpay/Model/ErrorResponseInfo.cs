﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    public class ErrorResponseInfo : BaseResultInfo
    {
        public int code { get; set; }

        public string msg { get; set; }

        public string sub_code { get; set; }

        public string sub_msg { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        public ResponseArgs args { get; set; }
    }

    public class ResponseArgs
    {
        public List<KeyValuePair<string, string>> arg { get; set; }
    }
}
