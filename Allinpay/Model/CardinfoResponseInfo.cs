using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    public class CardCardinfoResponse : BaseResponse<CardCardinfoResponseInfo>
    {
        public CardCardinfoResponseInfo card_cardinfo_get_response { get; set; }

        protected override CardCardinfoResponseInfo GetResultInfo()
        {
            return card_cardinfo_get_response;
        }
    }

    public class CardCardinfoResponseInfo : BaseResultInfo
    {
        public CardInfo card_info { get; set; }

        /// <summary>
        /// 得到卡下的金额帐户
        /// </summary>
        /// <param name="productionId">产品号</param>
        /// <returns></returns>
        public CardProductInfo GetMoneyAccount(string productionId)
        {
            if (card_info != null && card_info.card_product_info_arrays != null && card_info.card_product_info_arrays.card_product_info != null)
            {
                foreach (CardProductInfo info in card_info.card_product_info_arrays.card_product_info)
                {
                    if (info.product_id == productionId)
                    {
                        return info;
                    }
                }
            }
            return null;
        }
    }
}
