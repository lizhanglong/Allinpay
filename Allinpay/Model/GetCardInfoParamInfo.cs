﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    /// <summary>
    ///  查询卡信息
    /// </summary>
    public class GetCardInfoParamInfo : BaseParamInfo
    {
        ///<summary>
        ///必须 卡号  19位卡号 
        ///</summary>
        public String card_id { get; set; }

        ///<summary>
        ///必须 密码,DES加密传输  6位数字密码的des加密值 时间+aop+原始密码
        ///</summary>
        public String password { get; set; }

        public override string method
        {
            get
            {
                return "allinpay.card.cardinfo.get";
            }
        }
    }
}
