﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    /// <summary>
    ///   修改卡密码
    /// </summary>
    public class CardChangePasswordParamInfo : BaseHandleOrderParamInfo
    {

        public override string method
        {
            get { return "allinpay.card.password.change"; }
        }

        ///<summary>
        ///必须 充值卡号  19位卡号 
        ///</summary>
        public String card_id { get; set; }

        ///<summary>
        ///必须 当前密码（修改前的密码）,DES加密   
        ///</summary>
        public String password { get; set; }

        ///<summary>
        ///必须 新密码（修改后的密码）,DES加密   
        ///</summary>
        public String new_password { get; set; }

    }
}
