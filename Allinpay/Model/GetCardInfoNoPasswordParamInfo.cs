﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    /// <summary>
    /// 查询卡信息(不验密码) !!提醒：订单号(仅仅是接口请求的标识，防止重复提交用的，没有业务含义)
    /// </summary>
    public class GetCardInfoNoPasswordParamInfo : BaseHandleOrderParamInfo
    {
        public override string method
        {
            get { return "allinpay.ppcs.cardinfo.get"; }
        }

        /// <summary>
        /// 19位卡号
        /// </summary>
        public string card_id { get; set; }
    }
}
