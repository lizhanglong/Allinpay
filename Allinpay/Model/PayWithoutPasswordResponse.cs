﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    public class PayWithoutPasswordResponse : BaseResponse<PayWithoutPasswordResponseInfo>
    {
        public PayWithoutPasswordResponseInfo card_paywithcontract_add_response { get; set; }

        protected override PayWithoutPasswordResponseInfo GetResultInfo()
        {
            return card_paywithcontract_add_response;
        }
    }
}
