﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay.Model
{
    public class PayResultInfo:BaseResultInfo
    {
        ///<summary>
        ///交易类型 01-网上支付 01
        ///</summary>
        public String type { get; set; }

        ///<summary>
        ///商户号 通联分配的交易商户  
        ///</summary>
        public String mer_id { get; set; }

        ///<summary>
        ///商户上送交易时间 20110201010101  
        ///</summary>
        public String mer_tm { get; set; }

        ///<summary>
        ///商户订单号 最长30位  
        ///</summary>
        public String mer_order_id { get; set; }

        ///<summary>
        ///支付币种 默认CNY代表人民币  
        ///</summary>
        public String pay_cur { get; set; }

        ///<summary>
        ///支付活动 10位数字 0000000001
        ///</summary>
        public String payment_id { get; set; }

        ///<summary>
        ///交易金额 100代表1元，以分为单位   
        ///</summary>
        public String amount { get; set; }

        ///<summary>
        ///卡号(DES加密) 19位数字卡号，加密 
        ///</summary>
        public String card_id { get; set; }

        ///<summary>
        ///商户上送的自定义信息   
        ///</summary>
        public String misc { get; set; }

        ///<summary>
        ///用户手续费 100代表一元，以分为单位  
        ///</summary>
        public String user_fee { get; set; }

        ///<summary>
        ///商户上送的用户名称  
        ///</summary>
        public String user_name { get; set; }

        ///<summary>
        ///商品名称   
        ///</summary>
        public String goods_name { get; set; }

        ///<summary>
        ///商品种类   
        ///</summary>
        public String goods_type { get; set; }

        ///<summary>
        ///商品号  
        ///</summary>
        public String goods_id { get; set; }

        ///<summary>
        ///商品备注   
        ///</summary>
        public String goods_misc { get; set; }

        ///<summary>
        ///交易状态 交易成功固定值1-成功，不成功的返回异常信息
        /// 0-处理中 1-成功 2-失败 
        ///</summary>
        public String stat { get; set; }

        ///<summary>
        ///成功支付流水号 通联返回的交易流水号
        ///</summary>
        public String pay_txn_id { get; set; }

        ///<summary>
        ///支付成功时间  
        ///</summary>
        public String pay_txn_tm { get; set; }


        /// <summary>
        /// 判断支付是否成功（stat==1）
        /// </summary>
        /// <returns></returns>
        public bool PaySuccess()
        {
            return stat == "1";
        }
    }
}
