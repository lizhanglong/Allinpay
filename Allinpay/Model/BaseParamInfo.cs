﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Allinpay;

namespace Allinpay.Model
{
    /// <summary>
    /// 基础请求参数
    /// </summary>
    public abstract class BaseParamInfo
    {
        protected BaseParamInfo()
        {
            app_key = AllinpayLib.AppKey;
            v = "1.0";
            sign_v = AllinpayLib.AppSignV ?? "1";
            timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
        }

        /// <summary>
        /// 请求接口时传输方法
        /// </summary>
        internal string HttpMethod { get; set; }

        /// <summary>
        /// AOP分配给应用的AppKey
        /// </summary>
        public string app_key { get; set; }

        /// <summary>
        /// API接口名称
        /// </summary>
        public abstract string method { get; }

        /// <summary>
        /// 时间戳，格式为yyyyMMddHHmmss，例如：20110125010101。API服务端允许客户端请求时间误差为30分钟。
        /// </summary>
        public string timestamp { get; set; }

        /// <summary>
        /// API协议版本，可选值:v1.0 。
        /// </summary>
        public string v { get; set; }

        /// <summary>
        /// API输入参数签名结果，使用dsa加密(签名后一定要对该参数赋值，后续验证返回结果会用到)
        /// </summary>
        internal string sign { get; set; }

        /// <summary>
        /// 签名版本号 1 ,2,3,4,5每次更新后+1递增
        /// </summary>
        public string sign_v { get; set; }

        /// <summary>
        /// 可选，指定响应格式。默认xml,目前支持格式为xml,json
        /// </summary>
        public string format { get; set; }

        //protected virtual Dictionary<string, string> GetBaseParamDict()
        //{
        //    var dict = new Dictionary<string, string>()
        //    {
        //    };
        //    return dict;
        //}

        public virtual Dictionary<string, string> GetParamDict()
        {
            var props = this.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => p.CanRead);
            var dict = new Dictionary<string, string>();
            foreach (PropertyInfo prop in props)
            {
                var val = prop.GetValue(this, null).ToStringEx();
                if (string.IsNullOrEmpty(val))
                {
                    continue;
                }
                dict.Add(prop.Name, val);
            }
            return dict;
        }
    }
}
