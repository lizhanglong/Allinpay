﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay
{
    /// <summary>
    /// 充值途径
    /// </summary>
    public enum TopUpWayType
    {
        现金 = 1,
        刷卡 = 2,
        支票 = 3,
        转账 = 4,
        其它 = 5,
        //银行卡网银 = 6,
        //组合 = 7
    }
}
