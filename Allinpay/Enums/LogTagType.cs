﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay
{
    public enum LogTagType
    {
        RequestParams,
        ResultContent,
        HttpException,
        CheckRequetsResult
    }
}
