﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Allinpay
{
    /// <summary>
    /// 卡状态	0-正常 1-挂失 2-冻结 3-作废
    /// </summary>
    public enum CardStatType
    {
        /// <summary>
        /// 0
        /// </summary>
        正常 = 0,

        /// <summary>
        /// 1
        /// </summary>
        挂失,

        /// <summary>
        /// 2
        /// </summary>
        冻结,

        /// <summary>
        /// 3
        /// </summary>
        作废
    }
}
