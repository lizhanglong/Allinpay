﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Allinpay
{
    /// <summary>
    /// 验证数据返回有问题（返回结果可能伪造）
    /// </summary>
    public class RequestResultException : AllinpayException
    {
        public RequestResultException(string msg) : base(msg) { }
        public RequestResultException(string msg, string logMsg) : base(msg, logMsg) { }
    }
}
