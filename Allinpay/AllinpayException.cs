﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Allinpay
{
    /// <summary>
    /// 通联支付所有异常基类
    /// </summary>
    public class AllinpayException : ApplicationException
    {
        /// <summary>
        /// 该内容可用于记录到日志，包含一些数据
        /// </summary>
        public string LogMsg { get; set; }

        public AllinpayException(string msg)
            : this(msg, null)
        {

        }
        public AllinpayException(string msg, string logMsg)
            : base(msg)
        {
            LogMsg = logMsg;
        }
    }
}
